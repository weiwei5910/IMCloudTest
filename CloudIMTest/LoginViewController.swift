//
//  LoginViewController.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/16.
//  Copyright © 2016年 ww. All rights reserved.
//

import UIKit

//extension UIView{
//    @IBInspectable var cornerRadius: CGFloat{
//        get {
//            return layer.cornerRadius
//        }
//        
//        set {
//            layer.cornerRadius = newValue
//            layer.masksToBounds = (newValue > 0)
//        }
//    }
//}

class LoginViewController: UIViewController, JSAnimatedImagesViewDataSource {

    @IBOutlet var loginStackView: UIStackView!
    
    @IBOutlet var wallpaperImageView: JSAnimatedImagesView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.wallpaperImageView.dataSource = self
        
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        // 隐藏导航栏
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animateWithDuration(1) { () -> Void in
            self.loginStackView.axis = UILayoutConstraintAxis.Vertical
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func animatedImagesNumberOfImages(animatedImagesView: JSAnimatedImagesView!) -> UInt {
        
        return 3
    }
    
    
    func animatedImagesView(animatedImagesView: JSAnimatedImagesView!, imageAtIndex index: UInt) -> UIImage! {
        return UIImage(named:  "image\(index + 1)")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
