//
//  RoundImageView.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/16.
//  Copyright © 2016年 ww. All rights reserved.
//

import UIKit

// 实时渲染Image
@IBDesignable
class RoundImageView: UIImageView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    // 渲染圆角
    @IBInspectable var cornerRaidiu: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRaidiu
            layer.masksToBounds = (cornerRaidiu > 0)
        }
    }
    
    // 描边
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    // 描边颜色
    @IBInspectable var borderColor: UIColor? {
        didSet{
            layer.borderColor = borderColor?.CGColor
        }
    }
    
    

}
