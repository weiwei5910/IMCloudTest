//
//  RegTableViewController.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/17.
//  Copyright © 2016年 ww. All rights reserved.
//

import UIKit

class RegTableViewController: UITableViewController {

    // 元组
//    var (userOk, pwdOk, emailOk) = (false, false, false)
    
    @IBOutlet var loginTextFields: [UITextField]!
    
    @IBOutlet var username: UITextBox!
    @IBOutlet var pwd: UITextBox!
    @IBOutlet var email: UITextBox!
    @IBOutlet var region: UITextBox!
    @IBOutlet var question: UITextBox!
    @IBOutlet var answer: UITextBox!
    
    var possibleInputs:Inputs = []
    
    // 检查必填
    func checkRequiredFiedl(){
        // 使用第三方组件，获取界面上所有组件
        /*
        self.view.runBlockOnAllSubviews { (subview) in
            if let subview = subview as? UITextField{
                if subview.text!.isEmpty{
                    print("文本框不能为空")
                }
            }
        }
         */
        
        //        let rightLabel = UILabel(frame: CGRectMake(0, 0, 100, 30))
        //        rightLabel.text = username.placeholder
        //        username.rightView = rightLabel
        //        username.rightViewMode = .Always
        
        
        for textField in loginTextFields {
            if textField.text!.isEmpty {
                print("必填项为空")
                self.errorNotice("必填字段为空")
            }
        }
        
        // 使用正则表达式
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        guard predicate.evaluateWithObject(email.text) else{
            self.errorNotice("E-Mail 格式不正确")
            return
        }
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = false
        
        self.title = "新用户注册"

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(RegTableViewController.doneBtnTap))
        self.navigationItem.rightBarButtonItem?.enabled = false
        
        let doneBtn = self.navigationItem.rightBarButtonItem
        
        let vl = AJWValidator(type: .String)
        vl.addValidationToEnsureMinimumLength(3, invalidMessage: "用户名至少3位")
        vl.addValidationToEnsureMaximumLength(15, invalidMessage: "最大为15位")
        // 添加验证
        self.username.ajw_attachValidator(vl)
        vl.validatorStateChangedHandler = { (newState: AJWValidatorState) -> Void in
            switch newState {
            // 有效
            case AJWValidatorState.ValidationStateValid:
                self.username.highlightState = UITextBoxHighlightState.Default
                self.possibleInputs.unionInPlace(Inputs.username)
//                self.userOk = true
            default:
                let errorMsg = vl.errorMessages.first as? String
                self.username.highlightState = UITextBoxHighlightState.Wrong(errorMsg!)
                self.possibleInputs.subtractInPlace(Inputs.username)
//                self.userOk = false
            }
            doneBtn?.enabled = self.possibleInputs.boolValue
//            doneBtn?.enabled = self.possibleInputs.isAllOK()
//            doneBtn?.enabled = self.userOk && self.pwdOk && self.emailOk
        }
        
        let vl2 = AJWValidator(type: .String)
        vl2.addValidationToEnsureMinimumLength(3, invalidMessage: "密码至少3位")
        vl2.addValidationToEnsureMaximumLength(15, invalidMessage: "最大为15位")
        // 添加验证
        self.pwd.ajw_attachValidator(vl2)
        vl2.validatorStateChangedHandler = { (newState: AJWValidatorState) -> Void in
            switch newState {
            case AJWValidatorState.ValidationStateValid:
                self.pwd.highlightState = UITextBoxHighlightState.Default
                self.possibleInputs.unionInPlace(Inputs.pwd)
//                self.pwdOk = true
            default:
                let errorMsg = vl2.errorMessages.first as? String
                self.pwd.highlightState = UITextBoxHighlightState.Wrong(errorMsg!)
                self.possibleInputs.subtractInPlace(Inputs.pwd)
//                self.pwdOk = false
            }
            doneBtn?.enabled = self.possibleInputs.boolValue
//            doneBtn?.enabled = self.possibleInputs.isAllOK()
//            doneBtn?.enabled = self.userOk && self.pwdOk && self.emailOk
        }
        
        
        let vl3 = AJWValidator(type: .String)
        vl3.addValidationToEnsureValidEmailWithInvalidMessage("Email格式不正确")
        self.email.ajw_attachValidator(vl3)
        vl3.validatorStateChangedHandler = { (newState: AJWValidatorState) -> Void in
            switch newState {
            case AJWValidatorState.ValidationStateValid:
                self.email.highlightState = UITextBoxHighlightState.Default
                self.possibleInputs.unionInPlace(Inputs.email)
//                self.emailOk = true
            default:
                let errorMsg = vl3.errorMessages.first as? String
                self.email.highlightState = UITextBoxHighlightState.Wrong(errorMsg!)
                self.possibleInputs.subtractInPlace(Inputs.email)
//                self.emailOk = false
            }
            doneBtn?.enabled = self.possibleInputs.boolValue
//            doneBtn?.enabled = self.possibleInputs.isAllOK()
//            doneBtn?.enabled = self.userOk && self.pwdOk && self.emailOk
        }
       
    }
    
    func doneBtnTap(){
//        checkRequiredFiedl()
        
//        let alert = UIAlertController(title: "提示", message: nil, preferredStyle: .Alert)
//        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
//        alert.addAction(action)
//        self.presentViewController(alert, animated: true, completion: nil)
        
        // 载入提示
        self.pleaseWait()
        
        // 建立用户的 AVObject
        let user = AVObject(className: "W_User")
        user["username"] = self.username.text
        user["pwd"] = self.pwd.text
        user["email"] = self.email.text
        user["region"] = self.region.text
        user["question"] = self.question.text
        user["answer"] = self.answer.text
        
        // 查询系统中是否已存在
        let query = AVQuery(className: "W_User")
        query.whereKey("username", equalTo: self.username.text)
        
        //执行查询
        query.getFirstObjectInBackgroundWithBlock { (username, e) in
            self.clearAllNotice()
            if username != nil {
                self.errorNotice("用户已注册")
                self.username.becomeFirstResponder()
                self.navigationItem.rightBarButtonItem?.enabled = false
            } else {
                user.saveInBackgroundWithBlock({ (succ, e) in
                    if succ {
                        self.successNotice("注册成功")
                        self.navigationController?.popViewControllerAnimated(true)
                    } else {
                        self.errorNotice(e.localizedDescription)
                    }
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
     */
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
