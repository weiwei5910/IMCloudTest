//
//  inputs.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/18.
//  Copyright © 2016年 ww. All rights reserved.
//

import Foundation

struct Inputs: OptionSetType {
    let rawValue: Int
    
    static let username = Inputs(rawValue: 1 << 0)
    static let pwd = Inputs(rawValue: 1 << 1)
    static let email = Inputs(rawValue: 1 << 2)
    
}

extension Inputs: BooleanType{
    var boolValue: Bool {
        return self.rawValue == 0b111
    }
}

// 判断是否全部输入
extension Inputs{
    func isAllOK() -> Bool {
//        return self == [.username, .pwd, .email]
//        return self.rawValue == 0b111
        
        let count = 3
        var found = 0
        for time in 0..<count{
            if self.contains(Inputs(rawValue: 1 << time)){
                found += 1
            }
        }
        return found == count
    }
}