//
//  AppDelegate.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/14.
//  Copyright © 2016年 ww. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RCIMUserInfoDataSource {

    var window: UIWindow?
    
    
    func connectServer(completion: ()->Void){
        
        // 初始化appKey
        RCIM.sharedRCIM().initWithAppKey("mgb7ka1nbzg3g")
        
        // 连接
        RCIM.sharedRCIM().connectWithToken("CTLynI+3cW2Alzty39plYmhy7plwbzEKIpc5J7ei+0EjKxdZsmh8XyzRkaww6E+8eBS7P1htP3eg7sf5OtFk+/NL8Gz6ElPw", success: { (_) in
            
            
            let currentUser = RCUserInfo(userId: "xiaowang", name: "小王", portrait: nil)
            RCIMClient.sharedRCIMClient().currentUserInfo = currentUser
            
            print("连接成功1")
            
            // 异步操作 调用主线程
            dispatch_async(dispatch_get_main_queue(), { 
                completion()
            })

            
            }, error: { (_) in
                print("连接失败")
        }) {
            print("token错误")
        }
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
        // 设置用户数据源代理
        RCIM.sharedRCIM().userInfoDataSource = self
        
        AVOSCloud.setApplicationId("U7IDl8nwfnesRjR8bkluwMKh-gzGzoHsz", clientKey: "kX22vWXhBq8hHJc0AXPj92tH")
        
    
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func getUserInfoWithUserId(userId: String!, completion: ((RCUserInfo!) -> Void)!) {
        let userInfo = RCUserInfo()
        userInfo.userId = userId
        
        switch userId {
        case "xiaowang":
            userInfo.name = "小王"
            userInfo.portraitUri = "http://img3.duitang.com/uploads/item/201408/15/20140815123528_yZFh4.png"
            
        case "xiaowang2":
            userInfo.name = "王巍"
            userInfo.portraitUri = "http://img.taopic.com/uploads/allimg/110906/1382-110Z611025585.jpg"
        default:
            print("无此用户")
        }
        
        return completion(userInfo)
    }

}

