//
//  ConversationListViewController.swift
//  CloudIMTest
//
//  Created by 王巍 on 16/5/15.
//  Copyright © 2016年 ww. All rights reserved.
//

import UIKit

class ConversationListViewController: RCConversationListViewController {


    let conVC = RCConversationViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.connectServer {
            // 设置显示类型
            self.setDisplayConversationTypes([
                RCConversationType.ConversationType_APPSERVICE.rawValue,
                RCConversationType.ConversationType_CHATROOM.rawValue,
                RCConversationType.ConversationType_CUSTOMERSERVICE.rawValue,
                RCConversationType.ConversationType_DISCUSSION.rawValue,
                RCConversationType.ConversationType_GROUP.rawValue,
                RCConversationType.ConversationType_PRIVATE.rawValue,
                RCConversationType.ConversationType_PUBLICSERVICE.rawValue,
                RCConversationType.ConversationType_SYSTEM.rawValue
            ])
            self.refreshConversationTableViewIfNeeded()
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // self.tabBarController?.tabBar.hidden = true
        
        let destVC = segue.destinationViewController as? ConversationViewController
        destVC?.targetId = conVC.targetId
        destVC?.conversationType = conVC.conversationType
        destVC?.title = conVC.title
        
    }
    
    
    override func onSelectedTableRow(conversationModelType: RCConversationModelType, conversationModel model: RCConversationModel!, atIndexPath indexPath: NSIndexPath!) {
        
//        conVC.targetId = model.targetId
//        conVC.title = model.conversationTitle
//        conVC.conversationType = RCConversationType.ConversationType_PRIVATE
//        
//        self.navigationController?.pushViewController(conVC, animated: true)
//        self.tabBarController?.tabBar.hidden = true
        
        conVC.targetId = model.targetId
        conVC.title = model.conversationTitle
        conVC.conversationType = RCConversationType.ConversationType_PRIVATE
        
        self.performSegueWithIdentifier("tapOnCell", sender: self)
    }

    @IBAction func showMenu(sender: UIBarButtonItem) {
        
        let items = [
            MenuItem(title: "客服", iconName: "serve", glowColor: UIColor.redColor(), index: 0),
            
            MenuItem(title: "通讯录", iconName: "contact", glowColor: UIColor.yellowColor(), index: 1)
            
        ]
        
        let menu = PopMenu(frame: self.view.bounds, items: items)
        menu.menuAnimationType = .NetEase
        
        if menu.isShowed {
            return
        }
        
        menu.didSelectedItemCompletion = {(selectedItem: MenuItem!) -> Void in
            print(selectedItem.index)
            
        }
        
        menu.showMenuAtView(self.view)
        
//        // KxMenu
//        var frame = sender.valueForKey("view")?.frame
//        frame?.origin.y = (frame?.origin.y)! + 30
//        
//        
//        KxMenu.showMenuInView(self.view, fromRect: frame!, menuItems: [
//            KxMenuItem("客服", image: UIImage(named: "serve"), target: self, action: #selector(ConversationListViewController.clickMenu)),
//            KxMenuItem("好友", image: UIImage(named: "contact"), target: self, action: #selector(ConversationListViewController.clickMenu))
//            
//            
//            ])
        
    }
    
    func clickMenu(){
        print("点击了第一项")
    }
}
